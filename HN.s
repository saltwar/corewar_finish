.name 		"Un Herisson neurastenique++"
.comment	"Pour vaincre zork et sauver l'univers"

entry:
	sti			r1, %:live_loop, %1
	sti			r1, %:spines, %1
	sti			r1, %:entry_l1, %1
	sti			r1, %:paws_spawn, %1
	sti			r1, %:tails, %1
	sti			r1, %:tails, %9
	sti			r1, %:tails, %17
	sti			r1, %:paws_4, %1
	sti			r1, %:paws_4, %1
	sti			r1, %:paws_3, %1
	sti			r1, %:paws_3, %9
	sti			r1, %:paws_2, %1
	ld			%0, r16
	fork		%:live_loop

entry_l1:
	live		%42
	ld			%36303630, r2
	ld			%0, r16
	fork		%:paws_spawn

################################################################################

spines:
	live		%42
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	st			r2, -308
	zjmp		%:spines

################################################################################

live_loop:
	live		%42
	zjmp		%:live_loop

################################################################################

paws_spawn:
	live	%4320423
	zjmp	%3
	fork	%:paws_spawn

tails:
	live	%4329034
	fork	%:paws_3
	live	%3401123
	fork	%:paws_2
	live	%4590543
	fork	%:paws_1
	ld		%0, r2
	ld		%251883523, r3
	ld		%0, r13
paws_4:
	live	%4930423
	ld		%0, r14
	zjmp	%:kureijiharinezumi_o_kogeki

paws_2:
	live	%4342342
	fork	%:entry_l1
	ld		%251883523, r2
	ld		%386101251, r3
	ld		%0, r15
	ld		%0, r16
	zjmp	%:kureijiharinezumi_o_kogeki

paws_3:
	live	%4239013
	fork	%:tails
	live	%4093282
	fork	%:live_loop
	ld		%4294902016, r2
	ld		%436432899, r3
	ld		%0, r14
	ld		%0, r15
	ld		%0, r16
	zjmp	%:kureijiharinezumi_o_kogeki

paws_1:
	ld		%57672192, r2
	ld		%318992387, r3
	ld		%0, r16
	zjmp	%:kureijiharinezumi_o_kogeki

kureijiharinezumi_o_kogeki:
	st		r2, 15
	st		r3, -1
paws_live:
	live	%0

################################################################################
