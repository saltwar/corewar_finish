/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_util.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/27 11:37:41 by aaverty           #+#    #+#             */
/*   Updated: 2016/08/28 04:59:35 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void			asm_check_extension(t_app *app, char **argv)
{
	int len;

	len = ft_strlen(argv[1]);
	if (len < 2 || argv[1][len - 1] != 's' || argv[1][len - 2] != '.')
		ERROR("Error : bad file type.\n");
	app = (void*)app;
}

unsigned int	asm_reverse_uint(unsigned int n)
{
	unsigned char	*t;

	t = (unsigned char*)&n;
	return (0
				| (unsigned int)t[0] << 24
				| (unsigned int)t[1] << 16
				| (unsigned int)t[2] << 8
				| (unsigned int)t[3]);
}

int				ft_atoi2(const char *nptr)
{
	size_t	i;
	int		res;
	char	neg;

	neg = 0;
	res = 0;
	i = 0;
	while (' ' == nptr[i] || '\t' == nptr[i] || '\f' == nptr[i]
		|| '\v' == nptr[i] || '\r' == nptr[i] || '\n' == nptr[i])
		i++;
	if ('-' == nptr[i])
	{
		neg = 1;
		i++;
	}
	else if ('+' == nptr[i])
		i++;
	while ('0' <= nptr[i] && '9' >= nptr[i])
	{
		if (res > (res * 10) + nptr[i] - '0')
			return (-1);
		res = (res * 10) + nptr[i++] - '0';
	}
	res = (neg ? -res : res);
	return (res);
}

void			asm_put_success(char *str1, char *str2)
{
	ft_putstr("\033[32m");
	ft_putstr(str1);
	ft_putstr(str2);
	ft_putstr("\033[0m\n");
}

char			*ft_strndup(char *str, int len)
{
	char	*rep;

	if ((rep = (char*)ft_memalloc(len + 1)) == NULL)
		return (NULL);
	ft_strncpy(rep, str, len);
	rep[len] = '\0';
	return (rep);
}
