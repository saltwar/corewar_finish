/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 23:26:02 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/01 16:01:55 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static void	ft_free2(t_app *app)
{
	t_btcode	*bt;
	int			i;

	while (app->btcode)
	{
		i = 0;
		bt = app->btcode;
		app->btcode = app->btcode->next;
		while (bt->cmd->param[i].str)
			free(bt->cmd->param[i++].str);
		free(bt->cmd);
		free(bt);
	}
}

static void	ft_free(t_app *app)
{
	t_nodes		*tmp;
	t_label		*lab;

	while (app->lst_line)
	{
		tmp = app->lst_line;
		app->lst_line = app->lst_line->next;
		free(tmp->line);
		free(tmp);
	}
	while (app->label)
	{
		lab = app->label;
		app->label = app->label->next;
		free(lab->name);
		free(lab);
	}
	ft_free2(app);
}

int			main(int argc, char **argv)
{
	t_app app;

	if (argc != 2)
		ERROR("Error : wrong number of arguments.\n");
	asm_init_app(&app);
	asm_check_extension(&app, argv);
	asm_read_file(&app, argv[1]);
	asm_parse(&app);
	asm_open_out_file(&app, argv);
	if (app.byte_count == 0)
		ERROR("Error : the program has no instruction.\n");
	asm_write_data(&app);
	free(app.cursor);
	ft_free(&app);
	return (0);
}
