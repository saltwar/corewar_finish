/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_error_label.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/30 07:24:51 by aaverty           #+#    #+#             */
/*   Updated: 2016/08/30 07:09:50 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		asm_is_label_char(char c)
{
	int			i;
	const char	lb[] = LABEL_CHARS;

	i = 0;
	while (lb[i])
	{
		if (c == lb[i])
			return (1);
		i++;
	}
	return (0);
}

void	asm_error_label(char **str, t_app *app, int line)
{
	int			i;
	char		*tmp;

	i = 0;
	tmp = *str;
	while (asm_is_label_char(tmp[i]))
		i++;
	if (i == 0)
		ERROR("Error : label without name, line %d.\n", line);
	if (tmp[i] == ':')
	{
		i++;
		asm_push_label(app, ft_strndup(*str, i));
		i += asm_dodge_space_tab(&tmp[i]);
		*str += i;
	}
}
