#* ************************************************************************** *#
#*                                                                            *#
#*                                                        :::      ::::::::   *#
#*   Makefile                                           :+:      :+:    :+:   *#
#*                                                    +:+ +:+         +:+     *#
#*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        *#
#*                                                +#+#+#+#+#+   +#+           *#
#*   Created: 2016/01/29 15:20:46 by aaverty           #+#    #+#             *#
#*   Updated: 2016/09/30 11:57:59 by aaverty          ###   ########.fr       *#
#*                                                                            *#
#* ************************************************************************** *#

.PHONY: all clean fclean re

NAME = corewar

all: $(NAME)

$(NAME):
	@make -C libft/
	@echo
	@make -C asm/
	@echo
	@make -C vm/

clean:
	@make clean -C libft/
	@echo
	@make clean -C asm/
	@echo
	@make clean -C vm/

fclean: clean
	@make fclean -C libft/
	@echo
	@make fclean -C asm/
	@echo
	@make fclean -C vm/

re: fclean all

test:
	@make test -C asm
	@echo
	@make test -C vm/test
