#!/bin/sh
tab=('HN.cor' 'Kappa.cor' 'Machine-gun.cor' 'MarineKing.cor' 'Misaka_Mikoto.cor' 'Rainbow_dash.cor' 'THUNDER.cor' 'Varimathras.cor' 'Wall.cor' '_.cor' '_honeybadger.cor' 'bee_gees.cor' 'big_feet.cor' 'bigzork.cor' 'casimir.cor' 'champ.cor' 'corelol.cor' 'darksasuke.cor' 'doge.cor' 'dubo.cor' 'fluttershy.cor' 'gedeon.cor' 'helltrain.cor' 'jinx.cor' 'justin_bee.cor' 'littlepuppy.cor' 'live.cor' 'loose.cor' 'meowluigi.cor' 'salamahenagalabadoun.cor' 'sam_2.0.cor' 'skynet.cor' 'terminator.cor' 'turtle.cor' 'ultimate-surrender.cor' 'youforkmytralala.cor' 'zork.cor')

if [ ! -z $1 ] && [ $1 = "help" ]
then
	echo "usage : [ ] [ -a [ n_champ_start ] [ n_champ_end ] ] [ help ]"
	exit
fi

make  -C ../../

echo

function isnum {
    echo "$1" | egrep -q '^[-+]?[0-9]+$'
}


if [ -z $1 ]
then
	read -p 'Combien de champions souhaitez vous executer ? [1 ... 4]: ' nb_champ
	if [ $nb_champ -lt 1 ] || [ $nb_champ -gt 4 ]
	then
		echo "\033[31mError : Wrong number of players !\033[00m"
		exit
	fi
	read -p "Souhaitez vous l'option verbose ? [Y/n] : " verbose
	read -p "Souhaitez vous l'option diff ? [Y/n] : " diff
fi

champs=('' '' '' '')
k=0

while [ -z $1 ] && [ $nb_champ -ge 1 ] && [ $nb_champ -le 4 ];
do
	select champ in "${tab[@]}"
	do
		echo $champ
		if [ -n $champ ];
		then
			champs[$k]='./champions/'$champ
			break
		fi
	done
	let "k = k + 1"
	echo
	let "nb_champ = nb_champ - 1"
done

Y="Y"
y="y"

if [ ! -z ${champs[0]} ]
then
	if [ $verbose = $Y ] || [ $verbose = $y ]
	then
		../corewar -v ${champs[0]} ${champs[1]} ${champs[2]} ${champs[3]}
	elif [ $diff = $Y ] || [ $diff = $y ]
	then
		if [ ! -e our ]
		then
			mkdir our
		fi
		if [ ! -e ref ]
		then
			mkdir ref
		fi
		our='./our/verbose'
		ref='./ref/verbose'
		../corewar -v ${champs[0]} ${champs[1]} ${champs[2]} ${champs[3]} > $our
		./corewar_zaz -v 5 ${champs[0]} ${champs[1]} ${champs[2]} ${champs[3]} > $ref
		diff $our $ref
		if [ $? -eq 1 ]
		then
			echo "\033[31mDIFF BETWEEN $our AND $ref ISN'T OK !\033[00m"
		else
			echo "\033[32mDIFF BETWEEN $our AND $ref IS OK !\033[00m"
		fi
	else
		../corewar ${champs[0]} ${champs[1]} ${champs[2]} ${champs[3]}
	fi
	exit
fi

a="-a"

if [ ! -z $1 ] && [ $1 = $a ] && [ ! -z $2 ] && [ ! -z $3 ]
then
	if [ $2 -ge 1 ] && [ $2 -le 37 ]
	then
		i=$2
	fi
	if [ $3 -ge 1 ] && [ $3 -le 37 ]
	then
		j=$3
	fi
elif [ $1 = $a ]
then
	if [ -z $2 ] && [ -z $3 ]
	then
		i=0
		j=37
	fi
else
	exit
fi


while [ -n $1 ] && [ $i -le $j ]
do
	if [ ! -e our ]
	then
		mkdir our
	fi
	if [ ! -e ref ]
	then
		mkdir ref
	fi
	our='our/'${tab[$i]}'_verbose'
	ref='ref/'${tab[$i]}'_verbose'
	../corewar \-v 'champions/'${tab[$i]} > $our
	./corewar_zaz \-v 5 $verbose 'champions/'${tab[$i]} > $ref
	diff $our $ref
	if [ $? -eq 1 ]
	then
		echo "\033[31mDIFF BETWEEN $our AND $ref ISN'T OK !\033[00m"
	else
		echo "\033[32mDIFF BETWEEN $our AND $ref IS OK !\033[00m"
	fi
	let "i = i + 1"
done
