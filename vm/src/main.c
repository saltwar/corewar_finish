/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 15:27:05 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/01 16:02:40 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		ft_free(t_app *app)
{
	t_instance	*tmp;
	t_instance	*tmp2;

	tmp = app->first_instance;
	while (tmp)
	{
		tmp2 = tmp;
		tmp = tmp->next;
		free(tmp2);
	}
	free(tmp);
}

int				main(int argc, char **argv)
{
	t_app		app;

	if (argc == 1)
		print_help();
	ft_bzero(&app, sizeof(t_app));
	app.ac = argc;
	app.av = argv;
	init_app(&app);
	if (app.flag_v5)
		loop_verbose(&app);
	else if (app.fl.g)
		loop_ncurses(&app);
	else if (app.fl.d)
		loop_dump(&app);
	else
		loop_verbose(&app);
	ft_free(&app);
	return (0);
}
