/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_arg.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:45:43 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:23:37 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static t_type	read_registre(t_app *app, unsigned char *addr)
{
	unsigned char	data;
	t_type			rt;

	ft_bzero(&rt, sizeof(t_type));
	data = get_mem(app, addr - app->arena.memory, 1).data;
	rt.reg_nbr = data;
	if (data > REG_NUMBER || data <= 0)
		return (rt);
	rt.data = app->current_instance->reg[data - 1];
	rt.r = &app->current_instance->reg[data - 1];
	rt.type = REGISTRE;
	rt.size = 4;
	return (rt);
}

static void		set_encoding_arg_dir_ind(t_app *app, t_arg *rt, int token)
{
	if (token == DIRECT)
	{
		rt->arg[rt->current_arg].size =
			app->instr[app->current_instr - 1].nbr_octet[rt->current_arg];
		if (rt->arg[rt->current_arg].size == 0)
			return ;
		rt->arg[rt->current_arg].data =
			get_mem(app, rt->addr + rt->pos - app->arena.memory,
				rt->arg[rt->current_arg].size).data;
		rt->pos += rt->arg[rt->current_arg].size;
		rt->arg[rt->current_arg].type = DIRECT;
	}
	else if (token == INDIRECT)
	{
		rt->arg[rt->current_arg].size = 2;
		rt->arg[rt->current_arg].data =
			get_mem(app, rt->addr + rt->pos - app->arena.memory,
				rt->arg[rt->current_arg].size).data;
		rt->pos += rt->arg[rt->current_arg].size;
		rt->arg[rt->current_arg].type = INDIRECT;
	}
}

static void		read_encoding_arg(t_app *app, t_arg *rt)
{
	if (!app->instr[app->current_instr - 1].nbr_octet[rt->current_arg])
		return ;
	if (((rt->code << rt->current_arg * 2) & 0xc0) == 0x40)
	{
		rt->arg[rt->current_arg] = read_registre(app, rt->addr + rt->pos);
		rt->pos++;
	}
	else if (((rt->code << rt->current_arg * 2) & 0xc0) == 0x80)
		set_encoding_arg_dir_ind(app, rt, DIRECT);
	else if (((rt->code << rt->current_arg * 2) & 0xc0) == 0xc0)
		set_encoding_arg_dir_ind(app, rt, INDIRECT);
}

void			print_arg(t_arg *arg)
{
	unsigned int	i;

	i = 0;
	while (i < 4)
	{
		if (arg->arg[i].type == REGISTRE)
			ft_printf("r%d", arg->arg[i].reg_nbr);
		i++;
	}
}

t_arg			*read_encoding_args(t_app *app)
{
	t_arg			*rt;

	rt = malloc(sizeof(t_arg));
	ft_bzero(rt, sizeof(t_arg));
	rt->current_arg = 0;
	rt->pos = 2;
	rt->addr = &app->arena.memory[app->current_instance->pc];
	rt->code = app->arena.memory[(app->current_instance->pc + 1) % MEM_SIZE];
	while (rt->current_arg < 4)
	{
		read_encoding_arg(app, rt);
		rt->current_arg++;
	}
	ft_memcpy(&app->current_instance->arg, rt, sizeof(t_arg));
	return (rt);
}
