/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm_st.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:53:26 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:30:12 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		vm_st(t_app *app, t_instance *inst)
{
	unsigned long	tmp;
	t_arg			arg;
	t_arg			*temp;

	tmp = 0;
	temp = read_encoding_args(app);
	ft_memcpy(&arg, temp, sizeof(t_arg));
	free(temp);
	if (arg.arg[0].type != REGISTRE || arg.arg[1].type == DIRECT ||
		arg.arg[2].size != 0 || arg.arg[3].size != 0 ||
		arg.arg[0].size == 0 || arg.arg[1].size == 0)
		return (calculate_move(&arg, 4, 2));
	ft_memcpy(&inst->arg, &arg, sizeof(t_arg));
	print_current_instr(app, inst);
	if (arg.arg[1].type == REGISTRE)
		*arg.arg[1].r = arg.arg[0].data;
	else if (arg.arg[1].type == INDIRECT)
	{
		tmp = inst->pc + (arg.arg[1].data % IDX_MOD);
		put_mem(app, tmp, arg.arg[0]);
	}
	return (arg.pos);
}
