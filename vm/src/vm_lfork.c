/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm_lfork.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:52:28 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:28:29 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		vm_lfork(t_app *app, t_instance *inst)
{
	t_instance		*new_inst;
	t_instance		tmp;

	create_instance(app);
	new_inst = app->last_instance;
	ft_memcpy(&tmp, new_inst, sizeof(t_instance));
	ft_memcpy(new_inst, app->current_instance, sizeof(t_instance));
	new_inst->pid = tmp.pid;
	new_inst->next = tmp.next;
	new_inst->previous = tmp.previous;
	new_inst->pc += get_mem(app, inst->pc + 1, 2).data;
	new_inst->tmp_instr = 0;
	new_inst->cycle = app->cycle;
	if (app->flag_v5)
		ft_printf("P %4d | %s %d (%d)\n", inst->pid + 1,
			app->instr[app->current_instr - 1].name,
			get_mem(app, inst->pc + 1, 2).data, new_inst->pc);
	new_inst->pc %= MEM_SIZE;
	return (3);
}
