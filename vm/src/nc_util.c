/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nc_util.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 21:41:47 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 21:42:20 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	nc_check_size_term(t_app *app, t_render *r)
{
	if (r->size_x != COLS || r->size_y != LINES)
	{
		clear();
		nc_init_window(app, r);
		nc_print_arena(app, r->w_left);
		exec_player(app);
	}
}

void	nc_set_hex(unsigned char nbr, char *ret)
{
	unsigned char	c;

	c = nbr / 16;
	c = (c <= 9) ? c + '0' : c + 'a' - 10;
	ret[0] = c;
	c = nbr % 16;
	c = (c <= 9) ? c + '0' : c + 'a' - 10;
	ret[1] = c;
	ret[2] = '\0';
}

int		nc_get_current_live(t_app *app)
{
	int		total_live_current_period;
	int		i;

	total_live_current_period = 0;
	i = 0;
	while (i < (int)app->nbr_player)
	{
		total_live_current_period += app->player[i].live_count;
		i++;
	}
	return (total_live_current_period);
}

void	nc_move_memory(t_app *app)
{
	unsigned char	*access;
	unsigned int	y;
	unsigned int	x;
	unsigned int	i;
	int				ty;

	y = 0;
	access = app->arena.access;
	while (y < 64)
	{
		x = 2;
		i = 0;
		while (x < 194)
		{
			ty = y * 64 + i;
			if (app->arena.access[ty] & 0xf0)
				app->arena.access[ty] -= 0x10;
			app->arena.access[ty] &= 0xf7;
			if (app->arena.live[ty])
				app->arena.live[ty]--;
			x += 3;
			i++;
		}
		y++;
	}
}

void	nc_start_cycle(t_app *app)
{
	exec_player(app);
	while (app->cycle < app->start_cycle)
	{
		app->cycle++;
		nc_move_memory(app);
		exec_player(app);
		if (app->cycle_to_die > CYCLE_TO_DIE)
			break ;
	}
}
