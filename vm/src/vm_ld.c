/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm_ld.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:52:08 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:27:19 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		vm_ld(t_app *app, t_instance *inst)
{
	t_arg	arg;
	int		tmp;
	t_arg	*temp;

	temp = read_encoding_args(app);
	ft_memcpy(&arg, temp, sizeof(t_arg));
	free(temp);
	if (arg.arg[0].type == REGISTRE || arg.arg[1].type != REGISTRE ||
		arg.arg[2].size != 0 || arg.arg[3].size != 0 ||
		arg.arg[0].size == 0 || arg.arg[1].size == 0)
		return (calculate_move(&arg, 4, 2));
	inst->carry = 0;
	if (arg.arg[0].type == DIRECT)
		tmp = arg.arg[0].data;
	else
		tmp = get_mem(app, inst->pc + (arg.arg[0].data % IDX_MOD), 4).data;
	*arg.arg[1].r = tmp;
	ft_memcpy(&inst->arg, &arg, sizeof(t_arg));
	inst->arg.arg[0].data = tmp;
	inst->arg.arg[0].size = 4;
	print_current_instr(app, inst);
	if ((*arg.arg[1].r) == 0)
		inst->carry = 1;
	return (arg.pos);
}
