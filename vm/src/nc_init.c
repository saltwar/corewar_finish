/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nc_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 21:41:28 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/30 16:16:37 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void	nc_init_menu_define(t_render *r)
{
	mvwprintw(r->w_right, 33, 4, "Live breakdown for current period :");
	mvwprintw(r->w_right, 38, 4, "Live breakdown for last period : ");
	mvwprintw(r->w_right, 45, 4, "Cycle before cycle to die : ");
	mvwprintw(r->w_right, 47, 4, "CYCLE_TO_DIE : ");
	mvwprintw(r->w_right, 49, 4, "Cycles/second limit : ");
	mvwprintw(r->w_right, 51, 4, "Cycle : ");
	mvwprintw(r->w_right, 53, 4, "Processes : ");
}

static void	nc_init_menu_player(t_app *app, t_render *r)
{
	int		i;
	int		l;
	int		j;

	i = -1;
	l = 4;
	while (++i < (int)app->nbr_player)
	{
		mvwprintw(r->w_right, l, 4, "Player %d : ", i + 1);
		wattron(r->w_right, COLOR_PAIR(i + 6));
		app->player[i].header.prog_name[38] = '\0';
		j = 0;
		while (app->player[i].header.prog_name[j] != '\n' && j < 38)
			j++;
		app->player[i].header.prog_name[j] = '\0';
		mvwprintw(r->w_right, l, 16, "%s",
			(char *)app->player[i].header.prog_name);
		mvwprintw(r->w_right, l + 1, 4, "-----------------------------------"
				"-----------------------------------------------------------"
				"--------------------------------------");
		wattron(r->w_right, COLOR_PAIR(30));
		mvwprintw(r->w_right, l + 2, 4, "Last live :                     ");
		mvwprintw(r->w_right, l + 3, 4, "Lives in current period :       ");
		l += 7;
	}
}

static void	init_const_text(t_app *app, t_render *r)
{
	int		i;

	wattron(r->w_right, COLOR_PAIR(30));
	if (!r->run)
		mvwprintw(r->w_right, 2, 60, "** PAUSED **	");
	else if (r->run)
		mvwprintw(r->w_right, 2, 60, "** RUNNING **	");
	i = 0;
	while (i < 132)
	{
		wattron(r->w_right, COLOR_PAIR(10));
		mvwprintw(r->w_right, 35, 4 + i, "%c", ' ');
		mvwprintw(r->w_right, 40, 4 + i, "%c", ' ');
		wattroff(r->w_right, COLOR_PAIR(10));
		i++;
	}
	wattron(r->w_right, COLOR_PAIR(30));
	nc_init_menu_define(r);
	nc_init_menu_player(app, r);
	wattroff(r->w_right, COLOR_PAIR(30));
}

void		nc_init_window(t_app *app, t_render *r)
{
	r->w_left = subwin(stdscr, 68, 199, 0, 0);
	r->w_right = subwin(stdscr, 68, 140, 0, 198);
	r->size_x = COLS;
	r->size_y = LINES;
	wattron(r->w_left, COLOR_PAIR(10));
	wattron(r->w_right, COLOR_PAIR(10));
	wborder(r->w_left, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wborder(r->w_right, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wattroff(r->w_left, COLOR_PAIR(10));
	wattroff(r->w_right, COLOR_PAIR(10));
	init_const_text(app, r);
}

void		nc_init(t_app *app, t_render *r)
{
	initscr();
	start_color();
	noecho();
	nodelay(stdscr, TRUE);
	curs_set(0);
	nc_init_color();
	nc_init_window(app, r);
}
