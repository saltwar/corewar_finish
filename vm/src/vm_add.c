/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm_add.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:49:21 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:25:34 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		vm_add(t_app *app, t_instance *inst)
{
	t_arg			arg;
	t_arg			*tmp;

	tmp = read_encoding_args(app);
	ft_memcpy(&arg, tmp, sizeof(t_arg));
	free(tmp);
	if (arg.arg[0].type != 1 || arg.arg[1].type != 1 ||
		arg.arg[2].type != 1 || arg.arg[3].size != 0)
		return (calculate_move(&arg, 4, 3));
	ft_memcpy(&inst->arg, &arg, sizeof(t_arg));
	print_current_instr(app, inst);
	inst->carry = 0;
	*(arg.arg[2].r) = arg.arg[0].data + arg.arg[1].data;
	if (*(arg.arg[2].r) == 0)
		inst->carry = 1;
	return (arg.pos);
}
