/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nc_menu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 21:41:41 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/30 15:19:36 by fdexheim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void	nc_menu_last_period(t_app *app, t_render *r)
{
	int		i;
	int		j;
	int		last;
	float	percent;

	last = 0;
	i = 0;
	percent = 0;
	while (i < (int)app->nbr_player)
	{
		percent = app->player[i].live_count * 132;
		if (r->total_live_current_period)
			percent /= r->total_live_current_period;
		j = 0;
		while (j < percent && j + last < 132)
		{
			wattron(r->w_right, COLOR_PAIR(i + 11));
			mvwprintw(r->w_right, 40, 4 + j + last, "%c", ' ');
			wattroff(r->w_right, COLOR_PAIR(i + 11));
			j++;
		}
		last += j;
		i++;
	}
	r->current_cycle_to_die = app->cycle_to_die;
}

static void	nc_menu_current_period(t_app *app, t_render *r)
{
	int		i;
	int		j;
	int		last;
	float	percent;

	i = 0;
	percent = 0;
	last = 0;
	r->total_live_current_period = nc_get_current_live(app);
	while (i < (int)app->nbr_player)
	{
		percent = app->player[i].live_count * 132;
		if (r->total_live_current_period)
			percent /= r->total_live_current_period;
		j = 0;
		while (j < percent && j + last < 132)
		{
			wattron(r->w_right, COLOR_PAIR(i + 11));
			mvwprintw(r->w_right, 35, 4 + j + last, "%c", ' ');
			wattroff(r->w_right, COLOR_PAIR(i + 11));
			j++;
		}
		last += j;
		i++;
	}
}

static void	nc_menu_player(t_app *app, t_render *r)
{
	int		i;
	int		l;

	i = 0;
	l = 4;
	while (i < (int)app->nbr_player)
	{
		mvwprintw(r->w_right, l + 2, 37, "%d     ",
			((int)app->player[i].last_live > 0) ?
				(int)app->player[i].last_live : 0);
		mvwprintw(r->w_right, l + 3, 37, "%d     ",
			(int)app->player[i].live_count);
		i++;
		l += 7;
	}
}

void		nc_menu(t_app *app, t_render *r)
{
	wattron(r->w_right, COLOR_PAIR(30));
	mvwprintw(r->w_right, 49, 27, "%d         ", r->cts);
	mvwprintw(r->w_right, 51, 12, "%d         ", app->cycle - 1);
	mvwprintw(r->w_right, 53, 16, "%d         ", app->nb_processes);
	mvwprintw(r->w_right, 47, 19, "%d      ", app->cycle_to_die);
	mvwprintw(r->w_right, 45, 32, "%d      ",
		app->cycle_to_die - (app->cycle - app->last_check));
	nc_menu_player(app, r);
	nc_menu_current_period(app, r);
	if (app->cycle - app->last_check + 1 == app->cycle_to_die)
		nc_menu_last_period(app, r);
	wattroff(r->w_right, COLOR_PAIR(30));
}
