/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arena.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 20:38:03 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 20:38:07 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	print_arena_line(t_app *app, unsigned char *memory,
	unsigned char *access, int j)
{
	unsigned int	i;

	i = 0;
	if (app)
		;
	while (i < 64)
	{
		print_hex(memory[j * 64 + i]);
		if (access[j * 64 + i] & 0xf0)
			access[j * 64 + i] -= 0x10;
		access[j * 64 + i] &= 0xf7;
		i++;
		if (i != 64)
			write(1, " ", 1);
	}
}

void	print_arena2(t_app *app)
{
	unsigned char	*memory;
	unsigned char	*access;
	unsigned int	j;

	j = 0;
	memory = app->arena.memory;
	access = app->arena.access;
	while (j < 64)
	{
		write(1, "0x", 2);
		print_hex((j * 64) / 256);
		print_hex((j * 64) % 256);
		write(1, " : ", 3);
		print_arena_line(app, memory, access, j);
		write(1, "\n", 1);
		j++;
	}
}
