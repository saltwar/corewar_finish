/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 16:21:01 by aaverty           #+#    #+#             */
/*   Updated: 2016/10/01 11:06:33 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	loop_verbose(t_app *app)
{
	while (app->cycle_to_die <= CYCLE_TO_DIE)
	{
		app->cycle++;
		exec_player(app);
	}
	app->cycle++;
	exec_player(app);
	put_winner(app);
	if (app->player[0].prog)
		free(app->player[0].prog);
	if (app->player[1].prog)
		free(app->player[1].prog);
	if (app->player[2].prog)
		free(app->player[2].prog);
	if (app->player[3].prog)
		free(app->player[3].prog);
}

void	loop_dump(t_app *app)
{
	if (!app->fl.g)
	{
		while (app->cycle <= app->start_cycle)
		{
			if (app->cycle_to_die > CYCLE_TO_DIE)
				break ;
			exec_player(app);
			app->cycle++;
		}
		print_arena2(app);
	}
}
