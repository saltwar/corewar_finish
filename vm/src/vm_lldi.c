/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm_lldi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaverty <aaverty@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/28 17:53:07 by aaverty           #+#    #+#             */
/*   Updated: 2016/09/28 18:29:36 by aaverty          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		vm_lldi(t_app *app, t_instance *inst)
{
	t_arg	arg;
	t_arg	*tmp;

	tmp = read_encoding_args(app);
	ft_memcpy(&arg, tmp, sizeof(t_arg));
	free(tmp);
	if (arg.arg[0].size == 0 || arg.arg[1].type == INDIRECT ||
		arg.arg[2].type != REGISTRE || arg.arg[3].size != 0 ||
		arg.arg[1].size == 0 || arg.arg[2].size == 0)
		return (calculate_move(&arg, 2, 3));
	ft_memcpy(&inst->arg, &arg, sizeof(t_arg));
	print_current_instr(app, inst);
	inst->carry = 0;
	if (arg.arg[0].type == INDIRECT)
		arg.arg[0].data = get_mem(app, inst->pc + arg.arg[0].data, 4).data;
	*arg.arg[2].r = get_mem(app, inst->pc +
		((arg.arg[0].data + arg.arg[1].data)), 4).data;
	if ((*arg.arg[2].r) == 0)
		inst->carry = 1;
	if (app->flag_v5)
		ft_printf("       | -> load from %d + %d = %d (with pc %d)\n",
			arg.arg[0].data, arg.arg[1].data, arg.arg[0].data + arg.arg[1].data,
			inst->pc + ((arg.arg[0].data + arg.arg[1].data) % IDX_MOD));
	return (arg.pos);
}
